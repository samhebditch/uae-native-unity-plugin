//
//  TTSPlugin.m
//  UnityTextToSpeech
//
//  Created by Sam Hebditch on 30/05/2019.
//  Copyright © 2019 Redweb. All rights reserved.
//

// importing whatever we might need
#import <Foundation/Foundation.h>
#import "TextToSpeechViewController.h"
#import <AppKit/NSSpeechSynthesizer.h>

@implementation TextToSpeechViewController

- (id) init
{
    self = [super init];
    _speechSynth = [[NSSpeechSynthesizer alloc] init];
    _speechSynth.delegate = self;
    return self;
}
- (void)ConfTTS: (const char *) _language pitchSpeak: (float)_pitch rateSpeak:(float)_rate
{
    _langCode = [NSString stringWithUTF8String:_language];
    _pitch = _pitch;
    _rate = _rate;
    //UnitySendMessage("TextToSpeech", "onMessage", "Setting Success");
}
- (void)StartSpeaking: (const char *) _text
{
    _speechText = [NSString stringWithUTF8String:_text];
    _speechSynth.rate = *(_rate);
    [_speechSynth startSpeakingString:_speechText];
}

-(void)StopSpeaking
{
    if([_speechSynth isSpeaking])
    {
        [_speechSynth stopSpeakingAtBoundary:NSSpeechImmediateBoundary];
        NSString *utterance = @"";
        [_speechSynth startSpeakingString:utterance];
         [_speechSynth stopSpeakingAtBoundary:NSSpeechImmediateBoundary];
    }
}
-(BOOL)currentlySpeaking
{
    return(_speechSynth.speaking);
}

- (void)speechSynthesizer:(NSSpeechSynthesizer *)sender didFinishSpeaking:(BOOL)finishedSpeaking{
    if(finishedSpeaking)
    {
        _FinishedSpeaking = true;
    }
    else
    {
        _FinishedSpeaking = false;
    }
}

@end
// This is where we set up all of the tagging and other bits and peices that Unity will hook into.
extern "C"{
    TextToSpeechViewController *ttView = [[TextToSpeechViewController alloc] init];
    void _TAG_StartSpeaking(const char *_text)
    {
        [ttView StartSpeaking:_text];
    }
    void _TAG_StopSpeaking()
    {
        [ttView StopSpeaking];
    }
    void _TAG_ConfTTS(const char * _language, float _pitch, float _rate)
    {
        [ttView ConfTTS: _language pitchSpeak:_pitch rateSpeak:_rate];
    }
    bool currentlySpeaking()
    {
       return [ttView currentlySpeaking];
    }
    bool finishedSpeaking()
    {
        return[ttView FinishedSpeaking];
    }
}
