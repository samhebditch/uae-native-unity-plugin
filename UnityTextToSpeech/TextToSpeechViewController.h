//
//  TTSPlugin.h
//  UnityTextToSpeech
//
//  Created by Sam Hebditch on 30/05/2019.
//  Copyright © 2019 Redweb. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TextToSpeechViewController : NSViewController <NSSpeechSynthesizerDelegate>
    @property NSSpeechSynthesizer *speechSynth;
    @property NSString *speechText;
    @property NSString *langCode;
    @property float *pitch;
    @property float *rate;
    @property bool FinishedSpeaking;
@end
typedef void(*UnityCallback)(char * message);

